#pragma once
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>

#pragma pack(push, 1)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)

enum read_status {
    READ_OK,
    READ_ERROR
};

enum  write_status {
    WRITE_OK,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum read_status from_bmp (FILE* in, struct image* img);

enum write_status to_bmp (FILE* out, const struct image* img);

