#include "bmp.h"
#include "file_io.h"
#include "rotate.h"
#include <stdbool.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3){
        printf("Wrong count of args");
        return 1;
    }

    //Чтение
    FILE* f_in = NULL;
    //bool open_status = open_file(&f_in, argv[1], "rb");
    if (!(open_file(&f_in, argv[1], "rb"))) {
        printf("Can't open the file for reading");
        return 1;
    }

    struct image old = {0};
    if (from_bmp(f_in, &old) != READ_OK) {
        printf("Error");
        return 1;
    }

    //Поворот
    struct image rot_image = rotate(old);

    //Запись
    FILE* f_out = NULL;
    if (!(open_file(&f_out, argv[2], "wb"))) {
        printf("Can't open the file for writing");
        return 1;
    }
    if (to_bmp(f_out, &rot_image) != WRITE_OK) {
        printf("WRITE_ERROR");
        return 1;
    }

    //Освобождение памяти
    free_img(&rot_image);
    free_img(&old);

    //Закрытие
    if (!(close_file(&f_in))) {
        printf("Something went wrong with closing the file for reading");
        return 1;
    }
    if (!(close_file(&f_out))) {
        printf("Something went wrong with closing the file for writing");
        return 1;
    }
    
    return 0;
}
