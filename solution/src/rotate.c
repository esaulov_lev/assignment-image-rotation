#include "rotate.h"
#include <stddef.h>

struct image rotate( struct image const img){
    struct pixel *tmp = malloc(sizeof(struct pixel) * img.height * img.width);
    struct pixel *data = img.data;

    for (uint32_t i = 0; i < img.height; i++) {
        for (uint32_t j = 0; j < img.width; j++) {
            *(tmp + j * img.height + (img.height - 1 - i)) = *(data + i * img.width + j);
        }
    }
    struct image img1 = {0};
    img1.width = img.height;
    img1.height = img.width;
    img1.data = tmp;
    return img1;
}
