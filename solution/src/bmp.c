#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#define TYPE 19778
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0

static struct bmp_header create_bmp (const uint64_t width, const uint64_t height) {
    struct bmp_header header = {
            .bfType = TYPE,
            .bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + height * (width % 4),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = DIB_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BPP,
            .biCompression = COMP,
            .biSizeImage =  width * height * sizeof(struct pixel) + height * (width % 4),
            .biXPelsPerMeter = X_PPM,
            .biYPelsPerMeter = Y_PPM,
            .biClrUsed = NUM_COLORS,
            .biClrImportant = IMP_COLORS
    };
    return header;
}

size_t padding(uint32_t width) {
    return width % 4;
    }

enum read_status from_bmp (FILE* in, struct image* img) {
    if (!in || !img) {
        return READ_ERROR;
    }

    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, in);

    *img = create_img(header.biWidth, header.biHeight);
    const size_t c_padding = padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        fread(&(img->data[i*(img->width)]), sizeof(struct pixel), img->width, in);
        if (fseek(in, c_padding, SEEK_CUR) != 0) {
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }
    struct bmp_header header = create_bmp(img->width, img->height);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    fseek(out, header.bOffBits, SEEK_SET);
    const size_t c_padding = padding(img->width);
    uint64_t padding_value = 0;
    for (size_t i = 0; i < img->height; i++) {
        size_t status = fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out);
        if (!status) {
            return WRITE_HEADER_ERROR;
        }
        
        if (fwrite(&padding_value, 1, c_padding, out) != c_padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
