#include <stdbool.h>
#include <stdio.h>

bool open_file (FILE** file, const char* filename, const char* mode) {
    *file = fopen(filename, mode);
    if (file == NULL) { return 0; }

    return 1;
}

bool close_file (FILE** file) {
    if (fclose(*file)) { return 0; }
    
    return 1;
}
